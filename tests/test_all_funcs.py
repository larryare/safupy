from Safupy.safupy import Safu
import os

salt = os.urandom(16)
password = 'fWW@7%Vp6rXnxKvQ@cmEJcj7Zy9d!h8yY7EVKor#%J^T'

s = Safu(salt, password)

def test_pw_hash_cmp():
    key = s.hashpw()
    assert s.cmphash(key)

def test_data_enc_dec():
    data = 'I like the stock'
    token = s.encryptdata(data)
    assert data == s.decryptdata(token)
